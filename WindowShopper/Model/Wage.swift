//
//  Wage.swift
//  WindowShopper
//
//  Created by Marko Bizjak on 09/09/2017.
//  Copyright © 2017 Marko Bizjak. All rights reserved.
//

import Foundation

class Wage {
    class func getHours(forWage wage: Double, andPrice price: Double) -> Int {
        return Int(ceil(price / wage))
    }
}
